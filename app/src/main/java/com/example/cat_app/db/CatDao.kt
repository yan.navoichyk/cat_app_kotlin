package com.example.cat_app.db

import androidx.lifecycle.LiveData;
import androidx.room.*


/**
 * The Room Magic is in this file, where you map a Java method call to an SQL query.
 *
 * When you are using complex data types, such as Date, you have to also supply type converters.
 * To keep this example basic, no types that require type converters are used.
 * See the documentation at
 * https://developer.android.com/topic/libraries/architecture/room.html#type-converters
 */

@Dao
interface CatDao {

    // LiveData is a data holder class that can be observed within a given lifecycle.
    // Always holds/caches latest version of data. Notifies its active observers when the
    // data has changed. Since we are getting all the contents of the database,
    // we are notified whenever any of the database contents have changed.
    @Query("SELECT * from cat_table ORDER BY cat ASC")
    fun getAlphabetizedWords(): LiveData<List<CatTable>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(catTable: CatTable)

    @Delete
    fun delete(catTable: CatTable)
}