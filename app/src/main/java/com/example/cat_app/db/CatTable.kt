package com.example.cat_app.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "cat_table")
data class CatTable(@PrimaryKey @ColumnInfo(name = "cat") val cat: String)