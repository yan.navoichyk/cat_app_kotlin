package com.example.cat_app.db

import androidx.room.Database
import androidx.room.RoomDatabase

/**
 * This is the backend. The database. This used to be done by the OpenHelper.
 * The fact that this has very few comments emphasizes its coolness.
 */
@Database(entities = [CatTable::class], version = 1)
abstract class RoomDatabase : RoomDatabase() {

    abstract fun catDao(): CatDao

}
