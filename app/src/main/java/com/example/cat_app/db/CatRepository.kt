package com.example.cat_app.db

import androidx.lifecycle.LiveData
import com.example.cat_app.db.CatTable
import com.example.cat_app.db.CatDao
import com.example.cat_app.db.RoomDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class CatRepository(roomDatabase: RoomDatabase) {
    private var catDAO: CatDao = roomDatabase.catDao()

    fun getCats(): LiveData<List<CatTable>> {
        return catDAO.getAlphabetizedWords()
    }

    fun insertCat(catTable: CatTable) {
        CoroutineScope(Dispatchers.IO).launch {
            catDAO.insert(catTable)
        }
    }

    fun deleteCat(catTable: CatTable) {
        CoroutineScope(Dispatchers.IO).launch {
            catDAO.delete(catTable)
        }
    }
}