package com.example.cat_app.favorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cat_app.R
import com.example.cat_app.db.CatTable
import com.example.cat_app.main.MainAdapter
import com.example.cat_app.model.Cat
import kotlinx.android.synthetic.main.item_favorite.view.*


class FavoriteListAdapter(private val onClickListener: OnClickListener) : RecyclerView.Adapter<FavoriteListAdapter.WordViewHolder>() {

    private var cats = emptyList<CatTable>()

    inner class WordViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val imageView = itemView.item_fav
        fun bind(catTable: CatTable) {
            Glide.with(imageView.context)
                .load(catTable.cat)
                .into(imageView);
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WordViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_favorite, parent, false)
        return WordViewHolder(view)
    }

    override fun onBindViewHolder(holder: WordViewHolder, position: Int) {
        val current = cats[position]
        holder.itemView.setOnClickListener {
            onClickListener.onClick(current)
        }
        holder.bind(current!!)
    }

    class OnClickListener(val clickListener: (cat: CatTable) -> Unit) {
        fun onClick(cat: CatTable) = clickListener(cat)
    }

    internal fun setWords(catTables: List<CatTable>) {
        this.cats = catTables
        notifyDataSetChanged()
    }

    override fun getItemCount() = cats.size
}