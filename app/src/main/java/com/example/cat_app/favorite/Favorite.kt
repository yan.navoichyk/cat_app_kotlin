package com.example.cat_app.favorite

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.PopupMenu
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.cat_app.model.CatViewModel
import com.example.cat_app.DownloadCat
import com.example.cat_app.MyApplication
import com.example.cat_app.R
import com.example.cat_app.db.CatTable
import com.example.cat_app.di.CatViewModelFactory
import com.example.cat_app.main.MainActivity
import javax.inject.Inject

class Favorite : AppCompatActivity() {
    lateinit var catViewModel: CatViewModel

    @Inject
    lateinit var catViewModelFactory: CatViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)
        (application as MyApplication).appComponent.inject(this)

        catViewModel = ViewModelProviders.of(this, catViewModelFactory)[CatViewModel::class.java]
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerview_item)
        val adapter = FavoriteListAdapter(FavoriteListAdapter.OnClickListener {
            val popupMenu = PopupMenu(this, recyclerView)
            popupMenu.menuInflater.inflate(
                R.menu.menu_favorites,
                popupMenu.menu
            )
            popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.download ->
                        DownloadCat().download(
                            it.cat,
                            this
                        )
                    R.id.delete ->
                        catViewModel.deleteCat(CatTable(it.cat))
                }
                true
            })
            popupMenu.show()
        })
        recyclerView.adapter = adapter
        catViewModel.getAllCats().observe(this, Observer { cats ->
            cats?.let {adapter.setWords(it)}
        })
        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_main -> {
                val intent = Intent(this@Favorite, MainActivity::class.java)
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent)
                return true
            }
            R.id.action_favorites -> {
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}
