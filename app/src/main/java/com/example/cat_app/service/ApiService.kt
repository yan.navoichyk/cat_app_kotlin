package com.example.cat_app.service

import com.example.cat_app.model.Cat
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("images/search?limit=5&size=small")
    fun getUsers(@Query("page") page:Int): Call<List<Cat>>
}