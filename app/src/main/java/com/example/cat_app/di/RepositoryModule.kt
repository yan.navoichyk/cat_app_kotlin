package com.example.cat_app.di

import com.example.cat_app.db.CatRepository
import com.example.cat_app.db.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun providesCatRepository(roomDatabase: RoomDatabase): CatRepository {
        return CatRepository(roomDatabase)
    }
}