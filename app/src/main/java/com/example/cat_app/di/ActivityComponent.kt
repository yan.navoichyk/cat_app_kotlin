package com.example.cat_app.di

import com.example.cat_app.favorite.Favorite
import com.example.cat_app.main.MainActivity
import dagger.Subcomponent

@Subcomponent
interface ActivityComponent {
    fun inject(activity: MainActivity)
    fun inject(activity: Favorite)
}