package com.example.cat_app.di

import android.app.Application
import androidx.room.Room
import com.example.cat_app.db.CatDao
import com.example.cat_app.db.RoomDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule(application: Application) {
    @Volatile
    private var INSTANCE: RoomDatabase? = null

    private var catApplication = application

    @Singleton
    @Provides
    fun getDatabase(
    ): RoomDatabase {
        // if the INSTANCE is not null, then return it,
        // if it is, then create the database
        return INSTANCE ?: synchronized(this) {
            val instance = Room.databaseBuilder(
                catApplication,
                RoomDatabase::class.java,
                "cat_database"
            )
                // Wipes and rebuilds instead of migrating if no Migration object.
                // Migration is not part of this codelab.
                .fallbackToDestructiveMigration()
                // .addCallback(WordDatabaseCallback(scope))
                .build()
            INSTANCE = instance
            // return instance
            instance
        }
    }

    @Singleton
    @Provides
    fun provideWordDao(db: RoomDatabase): CatDao{
        return db.catDao()
    }
    /*@Singleton
    @Provides
    fun getDatabase(context: Context): WordRoomDatabase {
        return Room.databaseBuilder(
            context,
            WordRoomDatabase::class.java, "WeatherApp-DB"
        ).build()
    }*/
}