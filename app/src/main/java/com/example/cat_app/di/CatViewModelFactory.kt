package com.example.cat_app.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.cat_app.db.CatRepository
import com.example.cat_app.model.CatViewModel
import javax.inject.Inject

class CatViewModelFactory @Inject constructor(private var catRepository: CatRepository) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CatViewModel(catRepository) as T
    }
}