package com.example.cat_app.di

import com.example.cat_app.favorite.Favorite
import com.example.cat_app.main.MainActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        DataBaseModule::class,
        RepositoryModule::class
    ]
)
interface AppComponent{

    fun inject(activity: MainActivity)
    fun inject(activity: Favorite)

}