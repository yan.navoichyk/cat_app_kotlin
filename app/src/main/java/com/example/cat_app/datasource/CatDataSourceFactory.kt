package com.example.cat_app.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.cat_app.model.Cat

class CatDataSourceFactory : DataSource.Factory<Int, Cat>() {
    val userLiveDataSource = MutableLiveData<CatDataSource>()
    override fun create(): DataSource<Int, Cat> {
        val userDataSource = CatDataSource()
        userLiveDataSource.postValue(userDataSource)
        return userDataSource
    }
}