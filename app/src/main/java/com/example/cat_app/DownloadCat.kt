package com.example.cat_app

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class DownloadCat {

    fun download(url: String, context: Context){
        Glide.with(context)
            .asBitmap()
            .load(url)
            .into(object : SimpleTarget<Bitmap?>(100, 100) {
                override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap?>?) {
                    saveImage(resource,url)
                }
            })
        Toast.makeText(context,"Image saved",Toast.LENGTH_SHORT).show()
    }

    private fun saveImage(image: Bitmap, name:String): String? {
        val getName = setNameImage(name)
        var savedImagePath: String? = null
        val file = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
                .toString()
        )
        val storageDir = File(file.absolutePath + "/Cats")
        var success = true
        if (!storageDir.exists()) {
            success = storageDir.mkdirs()
        }
        if (success) {
            val imageFile = File(storageDir, getName)
            savedImagePath = imageFile.getAbsolutePath()
            try {
                val fOut: OutputStream = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        return savedImagePath
    }

    private fun setNameImage(name: String) : String{
        val arrayName = name.split("/")
        return arrayName[arrayName.size-1]
    }


}