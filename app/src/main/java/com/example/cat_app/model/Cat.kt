package com.example.cat_app.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Cat (

    val id: String,
    val url: String

): Parcelable