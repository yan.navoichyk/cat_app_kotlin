package com.example.cat_app.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.cat_app.db.CatRepository
import com.example.cat_app.db.CatTable

class CatViewModel(private var catRepository: CatRepository): ViewModel() {

    fun getAllCats(): LiveData<List<CatTable>> {
        val allCats: LiveData<List<CatTable>> = catRepository.getCats()
        return allCats
    }
    fun addNewCat(cat: CatTable) {
        catRepository.insertCat(cat)
    }

    fun deleteCat(cat: CatTable) {
        catRepository.deleteCat(cat)
    }

}