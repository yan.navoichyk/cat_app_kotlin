package com.example.cat_app.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.cat_app.datasource.CatDataSource
import com.example.cat_app.datasource.CatDataSourceFactory
import com.example.cat_app.model.Cat

class MainViewModel : ViewModel() {
    var catPagedList: LiveData<PagedList<Cat>>
    private var liveDataSource: LiveData<CatDataSource>
    init {
        val itemDataSourceFactory = CatDataSourceFactory()
        liveDataSource = itemDataSourceFactory.userLiveDataSource
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setPageSize(CatDataSource.PAGE_SIZE)
            .build()
        catPagedList = LivePagedListBuilder(itemDataSourceFactory, config)
            .build()
    }
}