package com.example.cat_app.main

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.PopupMenu
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.cat_app.*
import com.example.cat_app.db.CatTable
import com.example.cat_app.di.CatViewModelFactory
import com.example.cat_app.favorite.Favorite
import com.example.cat_app.model.CatViewModel

import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

   lateinit var catViewModel: CatViewModel

    @Inject
    lateinit var catViewModelFactory: CatViewModelFactory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        (application as MyApplication).appComponent.inject(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    1
                );
            }
        }
        catViewModel = ViewModelProviders.of(this, catViewModelFactory)[CatViewModel::class.java]
        catViewModel
        val adapter =
            MainAdapter(MainAdapter.OnClickListenerCat {
                val popupMenu = PopupMenu(this, recyclerView)
                popupMenu.menuInflater.inflate(
                    R.menu.menu_all,
                    popupMenu.menu
                )
                popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.download ->
                            DownloadCat().download(
                                it.url,
                                this
                            )
                        R.id.favorite ->
                            catViewModel.addNewCat(CatTable(it.url))
                    }
                    true
                })
                popupMenu.show()
            })
        recyclerView.layoutManager = LinearLayoutManager(this)
        val itemViewModel = ViewModelProviders.of(this)
            .get(MainViewModel::class.java)
        itemViewModel.catPagedList.observe(this, Observer {
            adapter.submitList(it)
        })
        recyclerView.adapter = adapter

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener {
            val intent = Intent(this@MainActivity, Favorite::class.java)
            startActivity(intent)
        }



    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_main -> {
                return true
            }
            R.id.action_favorites -> {
                val intent = Intent(this@MainActivity, Favorite::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
