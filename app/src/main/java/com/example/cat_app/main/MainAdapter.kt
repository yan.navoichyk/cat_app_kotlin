package com.example.cat_app.main

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cat_app.R
import com.example.cat_app.model.Cat
import kotlinx.android.synthetic.main.item_user.view.*


class MainAdapter(private val onClickListener: OnClickListenerCat) : PagedListAdapter<Cat, MainAdapter.CatsViewHolder>(
    USER_COMPARATOR
) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatsViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_user, parent, false)
        return CatsViewHolder(view)
    }
    override fun onBindViewHolder(holder: CatsViewHolder, position: Int) {
        val user = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListener.onClick(user!!)
        }
        holder.bind(user!!)
    }
    class CatsViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        private val imageView = view.user_avatar
        fun bind(cat: Cat) {
            Glide.with(imageView.context)
                .load(cat.url)
                .into(imageView);
        }
    }

    class OnClickListenerCat(val clickListener: (cat: Cat) -> Unit) {
        fun onClick(cat: Cat) = clickListener(cat)
    }

    companion object {
        private val USER_COMPARATOR = object : DiffUtil.ItemCallback<Cat>() {
            override fun areItemsTheSame(oldItem: Cat, newItem: Cat): Boolean =
                oldItem.id == newItem.id
            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Cat, newItem: Cat): Boolean =
                newItem == oldItem
        }
    }
}