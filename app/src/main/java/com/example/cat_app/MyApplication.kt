package com.example.cat_app

import android.app.Application
import com.example.cat_app.di.AppComponent
import com.example.cat_app.di.DaggerAppComponent
import com.example.cat_app.di.DataBaseModule

open class MyApplication : Application() {
    companion object {
        lateinit var instance: MyApplication
    }
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        instance = this

        appComponent = DaggerAppComponent
            .builder()
            .dataBaseModule(DataBaseModule(this))
            .build()
    }

}